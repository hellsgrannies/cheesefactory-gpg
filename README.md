# cheesefactory-gpg

-----------------

#### A simple GPG wrapper for python_gnupg.
[![PyPI Latest Release](https://img.shields.io/pypi/v/cheesefactory-gpg.svg)](https://pypi.org/project/cheesefactory-gpg/)
[![PyPI status](https://img.shields.io/pypi/status/cheesefactory-gpg.svg)](https://pypi.python.org/pypi/cheesefactory-gpg/)
[![PyPI download month](https://img.shields.io/pypi/dm/cheesefactory-gpg.svg)](https://pypi.python.org/pypi/cheesefactory-gpg/)
[![PyPI download week](https://img.shields.io/pypi/dw/cheesefactory-gpg.svg)](https://pypi.python.org/pypi/cheesefactory-gpg/)
[![PyPI download day](https://img.shields.io/pypi/dd/cheesefactory-gpg.svg)](https://pypi.python.org/pypi/cheesefactory-gpg/)

### Main Features

**Note:** _This package is still in beta status. As such, future versions may not be backwards compatible and features may change. Parts of it may even be broken._

Coming soon.
